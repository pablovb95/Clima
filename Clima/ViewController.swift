//
//  ViewController.swift
//  Clima
//
//  Created by Catalina on 25/04/2021.
//

import UIKit

class ViewController: UIViewController {
   
    
    @IBOutlet weak var temperatura: UILabel!
    
  
    @IBOutlet weak var txtBuscar: UITextField!
    
   
    @IBOutlet weak var imgFondo: UIImageView!
    
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       

        
        
    }
    
    
    
    @IBAction func buscar(_ sender: Any) {
        var ruta = "dia nubes dispersas"
        print(txtBuscar.text)
        var temp = "Soleado"
        let url = "https://api.openweathermap.org/data/2.5/weather?q=\(txtBuscar.text ?? "Valencia")&units=metric&appid=b2161a770849e1d03b758475b72d053d&lang=es"
        let task = URLSession.shared.dataTask(with: URL(string: url)!, completionHandler: { data, response, error in
            guard let data = data, error == nil else {
                return
            }
            
            var result: Estructura?
            do {
                result = try JSONDecoder().decode(Estructura.self, from: data)
            }
            catch{
                print("error")
            }
            
            guard let json = result else {
                return
            }
            
            print(json.name)
            print(json.main.temp)
            print(json.weather[0].weatherDescription)
            temp = "\(json.weather[0].weatherDescription) \(json.main.temp)"
            
            
            let timeFormatter = DateFormatter()
            timeFormatter.dateFormat = "HH"
            let timeString = timeFormatter.string(from: Date())
            var timeInt = Int(timeString) ?? 1 + (json.timezone/3600)
            if timeInt > 23 {
                timeInt -= 24
            }
            
            if timeInt >= 10 && timeInt <= 20  {
                ruta = "dia \(json.weather[0].weatherDescription)"
            }else{
                ruta = "noche \(json.weather[0].weatherDescription)"
               
            }
              print(ruta)
        })
        task.resume()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2){
            self.imgFondo.image = UIImage(named: ruta)

            self.temperatura.text = "\(temp) C"
        }
    }
}
    


struct Estructura: Codable {
    let coord: Coord
    let weather: [Weather]
    let base: String
    let main: Main
    let visibility: Int
    let wind: Wind
    let clouds: Clouds
    let dt: Int
    let sys: Sys
    let timezone, id: Int
    let name: String
    let cod: Int
}


struct Clouds: Codable {
    let all: Int
}


struct Coord: Codable {
    let lon, lat: Double
}

struct Main: Codable {
    let temp, feelsLike, tempMin, tempMax: Double
    let pressure, humidity: Int

    enum CodingKeys: String, CodingKey {
        case temp
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure, humidity
    }
}


struct Sys: Codable {
    let type, id: Int
    let country: String
    let sunrise, sunset: Int
}


struct Weather: Codable {
    let id: Int
    let main, weatherDescription, icon: String

    enum CodingKeys: String, CodingKey {
        case id, main
        case weatherDescription = "description"
        case icon
    }
}

struct Wind: Codable {
    let speed: Double
    let deg: Int
}

  
        
